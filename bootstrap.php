<?php

require 'vendor/autoload.php';
require 'config.php';

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Guzzle\Http\Client;
use Nemrtvej\SZTG\Factory\PointsFactory;
use Nemrtvej\SZTG\Parser\StatsParser;
use Nemrtvej\SZTG\Facade\StatsFacade;


$entityPaths = array(__DIR__."\src\Entity");

$config = Setup::createAnnotationMetadataConfiguration(
    $entityPaths,
    true,
    null,
    null,
    false
    );

$parser = new StatsParser();
$client = new Client('http://www.serialzone.cz/tradegame/');

$em = EntityManager::create($conn, $config);

$pointsFactory = new PointsFactory($em);
$facade = new StatsFacade($client, $em, $pointsFactory, $parser);
