<?php

namespace Nemrtvej\SZTG\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;


/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 * @ORM\Entity()
 * @ORM\Table(name="points")
 */
class Points 
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $points;

    /**
     * @var Show
     *
     * @ORM\ManyToOne(targetEntity="Show", inversedBy="points")
     * @ORM\JoinColumn(name="show_id", referencedColumnName="id", onDelete="cascade")
     */
    private $show;

    function __construct(Show $show, $points)
    {
        $this->setDate(new DateTime());
        $this->setShow($show);
        $this->setPoints($points);
    }


    /**
     * @param \DateTime $date
     */
    public function setDate(DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $points
     */
    public function setPoints($points)
    {
        $this->points = (int) $points;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param Show $show
     */
    public function setShow(Show $show)
    {
        $this->show = $show;
    }

    /**
     * @return Show
     */
    public function getShow()
    {
        return $this->show;
    }

    public function dump()
    {
        return sprintf('%s: %s',
            $this->getShow()->dump(),
            $this->getPoints()
        );
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


}