<?php

namespace Nemrtvej\SZTG\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 *
 * @ORM\Entity()
 * @ORM\Table(name="shows", indexes={@ORM\Index(name="search_idx", columns={"szId"})})
 */
class Show 
{

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $szId;


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Points", mappedBy="show_id")
     */
    private $points;

    function __construct($name, $szId)
    {
        $this->setName($name);
        $this->setSzId($szId);
    }


    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $szId
     */
    public function setSzId($szId)
    {
        $this->szId = $szId;
    }

    /**
     * @return string
     */
    public function getSzId()
    {
        return $this->szId;
    }

    public function dump()
    {
        return sprintf('%s: %s (%s)',
            $this->getId(),
            $this->getName(),
            $this->getSzId()
        );
    }
}