<?php


namespace Nemrtvej\SZTG\Facade;

use Doctrine\ORM\EntityManager;
use Guzzle\Http\Client;
use Nemrtvej\SZTG\Exception\PageNotObtainedException;
use Nemrtvej\SZTG\Factory\PointsFactory;
use Nemrtvej\SZTG\Parser\StatsParser;


/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class StatsFacade 
{
    /** @var Client */
    private $client;

    /** @var EntityManager */
    private $em;

    /** @var PointsFactory  */
    private $pointsFactory;

    /** @var StatsParser */
    private $parser;

    public function __construct(Client $client, EntityManager $em, PointsFactory $pointsFactory,StatsParser $parser)
    {
        $this->client = $client;
        $this->em = $em;
        $this->pointsFactory = $pointsFactory;
        $this->parser = $parser;
    }


    public function run()
    {
        $pageSource = $this->getContent();
        $data = $this->parser->parse($pageSource);
        foreach ($data as $point) {
            $points = $this->pointsFactory->createPoint($point);
            $this->em->persist($points);
        }

        $this->em->flush();
    }

    private function getContent()
    {
        $request = $this->client->get();
        $response = $request->send();

        if ($response->getStatusCode() !== 200) {
            throw new PageNotObtainedException('System was unable to fetch requested page.');
        }

        return $response->getBody(true);
    }
}