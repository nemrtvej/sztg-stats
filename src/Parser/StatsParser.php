<?php

namespace Nemrtvej\SZTG\Parser;

use Nemrtvej\SZTG\Exception\PageMalformedException;

/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class StatsParser
{

    /**
     * Parses given sourceData into array with information about shows.
     * Array consists of arrays with keys: szId,showName and points.
     *
     * @param $sourceData
     * @return array
     * @throws \Nemrtvej\SZTG\Exception\PageMalformedException
     */
    public function parse($sourceData)
    {
        $output = [];

        //$shows = explode('<ul class="light" ', $sourceData);
        $shows = preg_split('/<ul class="(light|dark)"/', $sourceData);
        if (!count($shows)) {
            throw new PageMalformedException('Unable to find different TV shows in given page source');
        }
        for ($i = 1; $i<count($shows); $i++) {
            $line = $shows[$i];

            $output[] =  array(
                'showName' => $this->getShowName($line),
                'szId' => $this->getSzId($line),
                'points' => $this->getPoints($line),
            );
        }

        return $output;
    }

    private function getShowName($line)
    {
        $foo = explode('<span class="vysilani">', $line);
        $bar = explode('>', $foo[0]);

        return $bar[count($bar)-1];
    }

    private function getSzId($line)
    {
        $foo = explode('href="http://www.serialzone.cz/serial/', $line);
        $bar = explode('/', $foo[1]);

        return $bar[0];
    }

    private function getPoints($line)
    {
        $foo = explode('</li>', $line);

        $bar = explode('> ', $foo[1]);

        $points = $bar[count($bar)-1];

        if ((int)$points === 0) {
            $matches = [];
            preg_match('#<font [^>]*>(\d+)</font>#', $points, $matches);
            $points = $matches[1];
        }

        return $points;
    }

}