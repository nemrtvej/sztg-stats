<?php

namespace Nemrtvej\SZTG\Exception;

use Exception;


/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class PageMalformedException extends Exception
{

}