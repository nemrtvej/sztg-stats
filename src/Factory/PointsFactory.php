<?php

namespace Nemrtvej\SZTG\Factory;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

use Nemrtvej\SZTG\Entity\Points;
use Nemrtvej\SZTG\Entity\Show;

/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class PointsFactory 
{
    /** @var \Doctrine\ORM\EntityManager  */
    private $em;

    /** @var EntityRepository */
    private $showRepository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Constructs new Point entity from given data. Uses same format, as Parser/StatsParser::parse provides.
     * If Point requires non-existing Show Entity, it is automatically persisted, unlike the Points.
     *
     * @param array $data
     * @return Points
     */
    public function createPoint(array $data)
    {
        $this->init();
        $szId = $data['szId'];
        $points = $data['points'];
        $showName = $data['showName'];

        $show = $this->showRepository->findOneBy(['szId' => $szId]);
        if (!$show) {
            $show = $this->createShow($showName, $szId);
            $this->em->persist($show);
        }

        $points = new Points($show, $points);

        return $points;
    }

    public function createShow($showName, $szId)
    {
        return new Show($showName, $szId);
    }

    private function init()
    {
        if ($this->showRepository === null) {
            $this->showRepository = $this->em->getRepository('Nemrtvej\SZTG\Entity\Show');
        }
    }
}